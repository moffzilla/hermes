# Project Hermes

# Leverage vRealize Automation 8.1's Action Based eXtensibility (ABX), Event Broker Service (EBS) & APIs for e-mail customized notifications

One of the most common request you probably get when working with vRealize Automation 8.1 8.X or vRealize Automation Cloud is, 
Can you customize email notifications ?
furthermore, could that be done with Action Based eXtensibility (ABX)?  
The short answer to both is "Yes"

In this blog I will show you how you could leverage **Extensibility Actions** (based on Python Scripting), **Event Broker Service (EBS)**, **Action Based eXtensibility (ABX)** and **vRealize Automation's APIs** for sending customized email notifications for any kind of deployments with single or multiple resources, Private, Hybrid or Public Cloud based, Custom Resources, Kubernetes resources ( Namespaces, PODs and Clusters ), etc. 
This is not a definitive implementation, but instead of, I would like to provide you with a functional/operational implementation that you could use as base from which you could customized further, e.g. you may want to introduce your own email template, add/remove fields or introduce your own secrets engine.
I will walk you through the code in the following steps but you could find the whole script here https://gitlab.com/moffzilla/hermes/

Also, let's define the minimum solution requirements for this exercise:

```
-     Email notification must include Deployment and resource detailed information
-     Email Subject ( Template including Deployment information )
-     Send notification with multiple Deployment scenarios: Single, Multiple Resources such as VMs, Kubernetes Namespaces, Custom Resources, Cloud Services, etc. )
-     Automatically Discover Destination Address ( end user requesting the deployment action )
-     HTML & Text Format for email body
-     Notifications must be for successful and unsuccessful requests
-     SMTP Connection must be with TLS
-     vRealize Automation Refresh Token and Email Server Password should be fetched from Secrets Manager Server
```

System pre-requisites too: 
 
```
-     vRealize Automation Version 8.1 or vRA Cloud with ABX On-Prem Enabled
-     vRealize Suite LifeCycle Manager (vRSLCM) 8.1
-     SMTP Server (e.g. office365.com or mailtrap.io)
-     Of Course, since we want to send email notifications on deployments events, make sure to have a Cloud Account, Zone, Projects, Profiles, Images, etc configure to deploy Blueprints, etc.
-     Please note that vRealize Automation Version 8.1 should be able to fetch the end user valid email address via VMware Identity Manager, you can review a valid email address exists for your end user by going to vRealize Automation Version 8.1's "User Settings", then "My Account"
       
           [Insert Media My Account Email]

```

And before we begin, let’s go over Some key concepts: 

**Event Broker Service (EBS):** The service that dispatches messages published by a producer to the subscribed consumers.

**Action Based eXtensibility (ABX):** provides a lightweight and flexible run-time engine interface where you can define small scriptable actions and configure them to initiate when events specified in extensibility subscriptions occur.

**Extensibility Action:** A streamlined script of code that can run after an event is triggered in a subscription. Extensibility actions are similar to workflows, but are more lightweight. Extensibility actions can customized from within vRealize Automation Cloud Assembly.
You can create these extensibility action scripts of code within vRealize Automation and assign them to subscriptions. 

**Subscriptions:** Indicates that a subscriber is interested in being notified about an event by subscribing to an event topic and defining the criteria that triggers the notification. Subscriptions link either extensibility actions or workflows to triggering events used to automate parts of the applications life cycle.

**Event Topic** Describes a set of events that have the same logical intent and the same structure. Every event is an instance of an event topic.

Ok, so having that out of the way, let's draft the steps that need to happen

# Configure our vRealize Suite LifeCycle Manager to store our vRealize Automation's Refresh Token & Email Server Login credentials.
 
vRealize Suite LifeCycle Manager's Locker feature, allows you to secure and manage passwords, certificates, and licenses for vRealize Suite product solutions and integrations.
Since vRealize Automation 8.X  deployments contains vRealize Suite LifeCycle Manager it is an easy integration and logical choice for password store.
(please note that we can disable the use of vRealize Suite LifeCycle Manager and replace it with local variables)

Login to vRealize Suite LifeCycle Manager with Admin rights (e.g. **admin@local**), Go to **Locker Section**,  **Passwords** , then add 2 entries as follows:
    **myLocalTest_Email_Password**
    
    [Insert Media Add Email Password]
    
and
    **myLocalTest_Refresh_Token**
    
    [Insert Media Add Email Password]


# Let's login to vRealize Automation 8.1 and create Extensibility Action (code) able to :

login to login vRealize Automation 8.1, Go to **Extensibility**,  **Actions** , then **Import**

    [Insert Media ABX Action]
    
Main Function: 

            handler
Dependency:  

            requests
            
            json2html
            
(This is equivalent as defining your pip Library requirements for your Python Script)

FaaS Provider: 
            
             On Prem
                
Default Inputs:

            eventType = inputs["eventType"] → CREATE_DEPLOYMENT | DESTROY_DEPLOYMENT |  UPDATE_DEPLOYMENT
            projectName = inputs["projectName"] → e.g. My-Project
            requestType = inputs["requestType"] → e.g. BLUEPRINT | CATALOG | …..
            deploymentId = inputs['deploymentId'] → e.g.  6b3a942c-ee64-47b1-b95f-31e6a1105cac
            userName = inputs["__metadata"]["userName"] → e.g. tony@coke.sqa-horizon.local
            orgId = inputs["__metadata"]["orgId"] → e.g. 85ed3aa6-6398-485f-a8b5-35e74d0be25c
            __metadata = {"orgId":"85ed3aa6-6398-485f-a8b5-35e74d0be25c","userName":"tony@coke.sqa-horizon.local","eventTopicId":"example-value-2"}

(This is useful for testing Manually and since we actually make calls to system API, make sure to replace those values with valid ones if you want to test manually) 


[ Code Major Steps]


# Finally let's create Event Broker Service (EBS)'s Subscription for a **Deployment completed** Event
       
 A Deployment completed Event (deployment.request.post), is fired after deployment is provisioned for both blueprint and catalog requests 
  
login to login vRealize Automation 8.1, Go to **Extensibility**,  **Subscriptions** , then **New Subscriptions**

Populate Input as follows:
    
    [Insert Media For New Subscription]

# Testing & Validating  

 At this point, all it is needed is to request a deployment of any resource, e.g. a simple VM Blueprint in the Project we configured our action
 
 Let's find a Blueprint
 Let's Deploy it
 once it is complete
 you can validate that our ABX Action was executed by going to **Extensibility**,  **Activity** , then **Action Runs**, 
  
  
  and you should receive email notification for this deployment
  
  Let's update the deployment
  
  Let's Delete it
  
 
 
 You could imagine that testing / validate multiple scenario could be time consuming however we should and must automate that process
for that you can take advantage of vRealize Automation 8.1's Code Stream to create a Testing / Validate Pipeline that runs multiple email notification scenarions
    
    [Insert Media Email-Notification Pipeline]

                